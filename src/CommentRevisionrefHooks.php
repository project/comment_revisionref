<?php

namespace Drupal\comment_revisionref;

use Drupal\comment\CommentInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Form\FormStateInterface;

class CommentRevisionrefHooks {

  /**
   * Implements hook_entity_prepare_form().
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function hookEntityPrepareForm(EntityInterface $entity, $operation, FormStateInterface $form_state) {
    if ($entity instanceof CommentInterface) {
      if ($operation === 'default') {
        $commentedEntity = $entity->getCommentedEntity();
        if ($fieldName = CommentRevisionref::applicableFieldName($entity->bundle(), $commentedEntity->bundle())) {
          if ($commentedEntity instanceof RevisionableInterface) {
            $entity->set($fieldName, [
              'target_id' => $commentedEntity->id(),
              'target_revision_id' => $commentedEntity->getRevisionId(),
            ]);
            $commentedEntity->setNewRevision(TRUE);
          }
        }
      }
    }
  }

}
